
#!/bin/bash

npm cache clean --force    # preface with sudo  if necessary
rm package-lock.json
rm -rf node_modules